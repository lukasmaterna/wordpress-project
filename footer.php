<footer class="main-footer">
    <div class="reviews-box">
    <!-- TrustBox widget - Micro Review Count -->
    <div class="trustpilot-widget" data-locale="en-GB" data-template-id="5419b6a8b0d04a076446a9ad" data-businessunit-id="5c45c16293a8f90001edbc6a" data-style-height="24px" data-style-width="100%" data-theme="light">
        <a href="https://uk.trustpilot.com/review/acornbusinessfinance.co.uk" target="_blank">Trustpilot</a>
    </div>
    <!-- End TrustBox widget -->
    </div>
    <div class="special-box">
        <h3>We’re proud to be supporters of these special causes</h3>
        <img src="wp-content/uploads/2019/04/shelter-image.png" style="height: 80px;">
    </div>
    <section class="bottom-section">
        <div class="content-wrapper">
            <div class="col">
                <h3>Get in touch</h3>
                <p>To discuss financing for your business please contact us by whichever channel you find most convenient.</p>
                <p>Please note, calls may be recorded for training and quality purposes.</p>
            </div>
            <div class="col footer-links-col">
                <ul>
                    <div class="footer-link">
                        <i class="fas fa-phone"></i> <a href="tel:+44 (0) 1242 395507">+44 (0) 1242 395507</a>
                    </div>
                    <div class="footer-link">
                        <i class="fas fa-envelope"></i> <a href="mailto:hello@acornbusinessfinance.co.uk">hello@acornbusinessfinance.co.uk</a>
                    </div>
                    <div class="footer-link">
                        <i class="fas fa-map-marker"></i> Pure Offices, Hatherley Lane, Cheltenham, GL51 6SH													
                    </div>
                    <div class="footer-link">
                        <i class="fab fa-linkedin"></i> <a href="https://www.linkedin.com/company/acorn-business-finance/" target="_blank">LinkedIn</a>
                    </div>
                    <div class="footer-link">
                        <i class="fab fa-twitter-square"></i> <a href="https://twitter.com/acornbizfinance" target="_blank">Twitter</a>
                    </div>
                    <div class="footer-link">
                        <i class="fab fa-facebook"></i> <a href="https://www.facebook.com/acornbizfinance/" target="_blank">Facebook</a>
                    </div>
                    <div class="footer-link">
                        <i class="fab fa-instagram"></i> <a href="https://www.instagram.com/acornbizfinance/" target="_blank">Instagram</a>
                    </div>
                </ul>
            </div>
            <div class="legal">
                <p>Acorn Business Finance is the trading name of SJT Corporate Finance Limited. SJT Corporate Finance is registered in England and Wales, number 10919713. Registered office, Windsor House, Bayshill Road, Cheltenham GL50 3AT. SJT Corporate Finance Limited is authorised and regulated by the Financial Conduct Authority, reference number 822913.</p>
            </div>
        </div>
    </section>
</footer>
<!--Start of Tawk.to Script (0.3.3)-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{};
var Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d8c7ab26c1dde20ed038d9c/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script (0.3.3)--><div class="wpf_wizard_container wpf_comment_container wpf_hide" data-html2canvas-ignore="true" id="wpf_login_container">
<div class="wpf_wizard_modal">
<a href="javascript:void(0)" class="wpf_login_close" data-dismiss="alert">×</a>    	<div class='wpfeedback_image-preview-wrapper'><img alt="WP Feedback" title="WP Feedback" id='wpfeedback_image-preview' src='wp-content/uploads/2019/07/Acorn_Tree_Logo.png' height='100'></div>
<div class="wpf_loader wpf_loader_wizard wpf_hide"></div>
<div id="login_form_content"><p><b>Dive straight into the feedback!</b></br>Login below and you can start commenting using your own user instantly</p><form id="wpf_login" method="post"><div class="wpf_user"><label for="username"></label><input id="username" placeholder="Username OR Email Address" type="text" name="username"></div><div class="wpf_password"><label for="password"></label><input id="password" placeholder="Password" type="password" name="password"></div><input type="hidden" id="wpf_security" name="wpf_security" value="1591201534" /><input type="hidden" name="_wp_http_referer" value="/" /><input class="wpf_submit_button" type="submit" value="Login and start commenting" name="submit"><p class="wpf_status"></p></form></div>            </div>
</div><script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/acornbusinessfinance.co.uk\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts_ver=5.1.4.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7_redirect_forms = {"821":{"page_id":"689","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","delay_redirect":"0","after_sent_script":"","thankyou_page_url":"https:\/\/acornbusinessfinance.co.uk\/flexible-funding-in-5-7-days\/"},"820":{"page_id":"","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","delay_redirect":"","after_sent_script":"","thankyou_page_url":""}};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/wpcf7-redirect/js/wpcf7-redirect-script.js'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/core.min_ver=1.11.4.js'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/widget.min_ver=1.11.4.js'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/mouse.min_ver=1.11.4.js'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/draggable.min_ver=1.11.4.js'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/slider.min_ver=1.11.4.js'></script>
<script type='text/javascript' src='wp-includes/js/jquery/jquery.ui.touch-punch_ver=0.2.2.js'></script>
<script type='text/javascript' src='https://acornbusinessfinance.co.uk/wp-admin/js/iris.min.js?ver=5.1.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpf_ajax_login_object = {"ajaxurl":"https:\/\/acornbusinessfinance.co.uk\/wp-admin\/admin-ajax.php","wpf_reconnect_icon":"https:\/\/acornbusinessfinance.co.uk\/wp-content\/plugins\/wpfeedback\/images\/wpf_reconnect.png","redirecturl":"https:\/\/acornbusinessfinance.co.uk","loadingmessage":"Sending user info, please wait..."};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/wpfeedback/js/wpf-ajax-login_ver=1.1.8.js'></script>
<script type='text/javascript' src='wp-content/themes/acorn-lending/assets/js/min/main-min_ver=1547581132.js'></script>
<script type='text/javascript' src='wp-includes/js/wp-embed.min_ver=5.1.4.js'></script>

<?php wp_footer(); ?>

<script src="https://my.hellobar.com/865795115a77fa277c4bd5a6a6f0ed5a5aa9bf96.js" type="text/javascript" charset="utf-8" async="async"></script>
</body>
</html><!-- WP Fastest Cache file was created in 0.84470295906067 seconds, on 07-01-20 11:51:00 -->