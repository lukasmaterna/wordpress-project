<?php

// CSS
function load_stylesheets() 
{
    wp_register_style('aakr9', get_template_directory_uri() . '/css/aakr9.css', array(), 1, 'all');
    wp_enqueue_style('aakr9');

    wp_register_style('6b3f0', get_template_directory_uri() . '/css/6b3f0.css', array(), 1, 'all');
    wp_enqueue_style('6b3f0');

    wp_register_style('custom', get_template_directory_uri() . '/css/custom.css', array(), 1, 'all');
    wp_enqueue_style('custom');
}

add_action('wp_enqueue_scripts', 'load_stylesheets');

// JS
function load_javascript() 
{
    wp_register_script('wpcf7-redirect-script', get_template_directory_uri() . '/js/wpcf7-redirect-script.js', array(), 1, 1, 1);
    wp_enqueue_script('wpcf7-redirect-script');
}

add_action('wp_enqueue_scripts', 'load_javascript');

// Menus
add_theme_support('menus');

register_nav_menus(
    array(
        'main-menu' => __('Main Menu', 'theme'),
    )
);