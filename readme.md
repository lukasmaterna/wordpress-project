# Wordpress Custom Theme Project

## Plugins:
- ACF: https://www.advancedcustomfields.com/

### Front Page Template Fields:
- Hero 
    - Image
    - Heading
    - Description
    - Button Text
    - Button Link
    - Button 2 Text
    - Button 2 Link
    - Bottom Section Text
    - Bottom Section Button Text
    - Bottom Section Button Link

- Single Column
    - Heading
    - Decription
