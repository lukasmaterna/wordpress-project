<?php get_header(); ?>


<header class="page-header">
    
    <section class="top-section" style="padding: 6px 0 0 0;">
        <div class="content-wrapper-large">
            
            <div class="main-logo" style="width: 215px !important;">
                <a href="index.html">
                    <img src="wp-content/uploads/2019/02/acorn-logo.png" style="max-height: 80px;">
                </a>
            </div>

            <div class="responsive-menu">
                <div class="hamburger-bar down"></div>
                <div class="hamburger-bar fade"></div>
                <div class="hamburger-bar up"></div>
            </div>

            <!-- START: MENU -->
            <nav class="main-navigation" role="navigation">
                <div class="menu-main-navigation-container">
                    <?php 
                    wp_nav_menu(
                        array(
                            'theme_location' => 'main-menu',
                            'menu' => 'menu-main-navigation',
                            'container' => 'ul',
                            'menu_class' => 'menu',
                        )
                    );
                    ?>
                </div>
            </nav>
            <!-- END: MENU -->
        </div>
    </section>



<!-- START: ACF HERO -->
<?php $hero = get_field('hero'); ?>

<section class="mid-section" style="background-image: url(<?php echo $hero['image']; ?>);">
    <div class="content-wrapper-medium">
        <div class="text-wrapper">
            <h1><?php echo $hero['heading']; ?></h1>
            <h2><?php echo $hero['description']; ?></h2>
            <a class="header-cta" href="<?php echo $hero['button_link']; ?>"><?php echo $hero['button_text']; ?></a>
            <a class="header-cta" href="<?php echo $hero['button_2_link']; ?>"><?php echo $hero['button_2_text']; ?></a>
            <br/><br/>
        </div>
    </div>
</section>

<section class="bottom-section">
    <div class="content-wrapper-large">
        <h3><?php echo $hero['bottom_section_text']; ?></h3>
        <a href="<?php echo $hero['bottom_section_button_link']; ?>" class="button"><?php echo $hero['bottom_section_button_text']; ?></a>
    </div>
</section>
<!-- END: ACF HERO -->

</header>

<!-- START: ACF SINGLE COLUMN -->
<?php $singleColumn = get_field('single_column'); ?>

<article id="main-content">
    <section class="text-only-section">
        <div class="content-wrapper">
            <h2><?php echo $singleColumn['heading']; ?></h2>
            <?php echo $singleColumn['description']; ?>
        </div>
    </section>
</article>
<!-- END: ACF SINGLE COLUMN -->

<?php get_footer(); ?>